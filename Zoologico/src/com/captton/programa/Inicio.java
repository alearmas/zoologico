package com.captton.programa;

import com.captton.zoo.*;

public class Inicio {

	public static void main(String[] args) {
	
		Perro can1 = new Perro("Nero", 5, 40);
		can1.ladrar();
		System.out.println(can1.getPeso());
		can1.comer(3);
		System.out.println(can1.getPeso());
		
		Pez fish1 = new Pez("Nemo", 2, 2);
		fish1.nadar();
		System.out.println(fish1.getPeso());
		fish1.comer(2);
		System.out.println(fish1.getPeso());
		
		Pajaro bird1 = new Pajaro("Fenix", 4, 12);
		bird1.volar();
		System.out.println(bird1.getPeso());
		bird1.comer(2);
		System.out.println(bird1.getPeso());
	}

}

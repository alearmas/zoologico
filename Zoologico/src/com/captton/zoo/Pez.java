package com.captton.zoo;

public class Pez extends Animales {

	public Pez(String nombre, float edad, float peso) {
		super(nombre, edad, peso);
	}

	public Pez() {
	}
	
	public void nadar() {
		System.out.println("�Que lindo es nadar!");
	}

	@Override
	public void comer(float peso) {
		this.peso = (float) (this.peso * 1.70);
		System.out.println("Ahora " + this.nombre + " esta pesando " + getPeso());
	}

}

package com.captton.zoo;

public class Perro extends Animales {

	public Perro(String nombre, float edad, float peso) {
		super(nombre, edad, peso);
	}

	public Perro() {
	}
	
	public void ladrar() {
		System.out.println("�Me relaja ladrar!");
	}

	@Override
	public void comer(float peso) {
		this.peso = (float) (this.peso * 1.50);
		System.out.println("Ahora " + this.nombre + " esta pesando " + getPeso());
	}
}

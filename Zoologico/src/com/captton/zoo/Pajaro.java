package com.captton.zoo;

public class Pajaro extends Animales {

	public Pajaro(String nombre, float edad, float peso) {
		super(nombre, edad, peso);
	}

	public Pajaro() {
	}
	
	public void volar() {
		System.out.println("�Desde arriba todo es bello!");
	}

	@Override
	public void comer(float peso) {
		this.peso = (float) (this.peso * 1.30);
		System.out.println("Ahora " + this.nombre + " esta pesando " + getPeso());
	}

}
